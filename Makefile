ARCH ?= x86_64
SDK ?= /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.13.sdk
VERSION_MIN ?= 10.13

MANIFEST = libraries Makefile os-x-toolkit.rb
TAR_FILE = os-x-toolkit-r`bzr revno`.tgz

SHELL = /bin/bash
PATH = /usr/bin:/bin:/usr/sbin:/sbin

tgz:
	rm -rf os-x-toolkit-*.tgz
	tar cfz ${TAR_FILE} ${MANIFEST}

release: tgz
	scp ${TAR_FILE} gc:sites/generalconsumption.org/armagetronad/download/os-x-toolkit

build-all:
	ruby os-x-toolkit.rb --arch ${ARCH} --sdk ${SDK} --version-min ${VERSION_MIN} --all

build-%:
	ruby os-x-toolkit.rb --arch ${ARCH} --sdk ${SDK} --version-min ${VERSION_MIN} $*

clean:
	rm -rf libraries/Frameworks libraries/i386 libraries/x86_64 libraries/ppc
