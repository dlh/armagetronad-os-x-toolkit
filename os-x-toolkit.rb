require "fileutils"
require "optparse"
require "pathname"
require "uri"

def all_libraries
  libs = {}

  libs["SDL"] = Builder.new { |config, arch|
    archive = fetch("https://github.com/SDL-mirror/SDL/archive/SDL-1.2.tar.gz")
    source = expand_tgz(archive)
    Dir.chdir(source) do
      apply_patch(path("patches/SDL1.patch"))
      config.env_variables(arch)
      system("./configure", "--prefix", install_path(arch), "--disable-video-x11")
      system("make install")
    end

    # Create the framework
    framework = path("libraries/Frameworks/SDL.framework", :clear_dir)
    FileUtils.mkdir_p(File.join(framework, "Versions/A"))
    FileUtils.cp(path("libraries/#{arch}/lib/libSDL.dylib"), File.join(framework, "Versions/A/SDL"))
    FileUtils.cp_r(path("libraries/#{arch}/include/SDL"), File.join(framework, "Versions/A/Headers"))
    system(
      "install_name_tool",
      "-id",
      "@rpath/SDL.framework/Versions/A/SDL",
      File.join(framework, "Versions/A/SDL")
    )
    symlink_relative(File.join(framework, "Versions/A"), File.join(framework, "Versions/Current"))
    symlink_relative(File.join(framework, "Versions/Current/Headers"), File.join(framework, "Headers"))
    symlink_relative(File.join(framework, "Versions/Current/SDL"), File.join(framework, "SDL"))
  }

  libs["SDL_image"] = Builder.new { |config, arch|
    archive = fetch("http://www.libsdl.org/projects/SDL_image/release/SDL_image-1.2.12.dmg")
    source = open_dmg(archive)
    copy_framework(source, "SDL_image")
    close_dmg(source)
  }

  libs["SDL_mixer"] = Builder.new { |config, arch|
    archive = fetch("http://www.libsdl.org/projects/SDL_mixer/release/SDL_mixer-1.2.12.dmg")
    source = open_dmg(archive)
    copy_framework(source, "SDL_mixer")
    close_dmg(source)
  }

  libs["zthread"] = Builder.new("lib/libzthread.a") { |config, arch|
    archive = fetch("http://prdownloads.sourceforge.net/zthread/ZThread-2.3.2.tar.gz")
    source = expand_tgz(archive)
    Dir.chdir(source) do
      apply_patch(path("patches/ZThread.patch"))
      config.env_variables(arch)
      system("./configure", "--prefix", install_path(arch), "--disable-shared")
      system("make install")
    end
  }

  libs["SDL2"] = Builder.new { |config, arch|
    archive = fetch("http://www.libsdl.org/release/SDL2-2.0.10.dmg")
    source = open_dmg(archive)
    framework = copy_framework(source, "SDL2")
    link_framework(framework, arch)
    close_dmg(source)
  }

  libs["SDL2_image"] = Builder.new { |config, arch|
    archive = fetch("http://www.libsdl.org/projects/SDL_image/release/SDL2_image-2.0.5.dmg")
    source = open_dmg(archive)
    framework = copy_framework(source, "SDL2_image")
    link_framework(framework, arch)
    close_dmg(source)
  }

  libs["SDL2_mixer"] = Builder.new { |config, arch|
    archive = fetch("http://www.libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.4.dmg")
    source = open_dmg(archive)
    framework = copy_framework(source, "SDL2_mixer")
    link_framework(framework, arch)
    close_dmg(source)
  }

  libs["libpng"] = Builder.new("lib/libpng.a") { |config, arch|
    archive = fetch("http://prdownloads.sourceforge.net/libpng/libpng-1.6.37.tar.gz?download")
    source = expand_tgz(archive)
    Dir.chdir(source) do
      config.env_variables(arch)
      system("./configure", "--prefix", install_path(arch), "--disable-shared")
      system("make install")
    end
  }

  libs["freetype"] = Builder.new("lib/libfreetype.a") { |config, arch|
    archive = fetch("http://download.savannah.gnu.org/releases/freetype/freetype-2.6.tar.gz")
    source = expand_tgz(archive)
    Dir.chdir(source) do
      config.env_variables(arch)
      system("./configure", "--prefix", install_path(arch), "--disable-shared")
      system("make") # Since 2.5.0, freetype-config isn't generated when doing just "make install".
      system("make install")
    end
  }

  libs["ftgl"] = Builder.new("lib/libftgl.a") { |config, arch|
    archive = fetch("http://prdownloads.sourceforge.net/ftgl/ftgl-2.1.3-rc5.tar.gz?download")
    source = expand_tgz(archive)
    Dir.chdir(source) do
      config.env_variables(arch)
      system("./configure", "--prefix", install_path(arch), "--disable-shared", "--with-ft-prefix=#{install_path(arch)}", "--disable-freetypetest")
      system("make install")
    end
  }

  libs["protobuf"] = Builder.new("lib/libprotobuf.a") { |config, arch|
    archive = fetch("https://github.com/protocolbuffers/protobuf/archive/v2.6.1.tar.gz")
    source = expand_tgz(archive)
    Dir.chdir(source) do
      system("PATH=/usr/local/bin:$PATH autoreconf -f -i")
      config.env_variables(arch)
      system("./configure", "--prefix", install_path(arch), "--disable-shared")
      system("make install")
    end
  }

  libs["boost"] = Builder.new("lib/libboost_system.a") { |config, arch|
    architecture_option = {
      "i386" => "x86",
      "x86_64" => "x86",
      "ppc" => "ppc"
    }[arch]

    address_model_option = {
      "i386" => "32",
      "x86_64" => "64",
      "ppc" => "32"
    }[arch]

    archive = fetch("https://dl.bintray.com/boostorg/release/1.71.0/source/boost_1_71_0.tar.gz")
    source = expand_tgz(archive)
    Dir.chdir(source) do
      config.env_variables(arch)
      system("./bootstrap.sh", "--prefix=#{install_path(arch)}")
      system("./b2 -d0 --with-system --with-thread architecture=#{architecture_option} address-model=#{address_model_option} link=static cxxflags=#{escape_sh(ENV["CXXFLAGS"])} cflags=#{escape_sh(ENV["CFLAGS"])} linkflags=#{escape_sh(ENV["LDFLAGS"])} install")
    end
  }

  libs["GLEW"] = Builder.new("lib/libGLEW.a") { |config, arch|
    archive = fetch("http://prdownloads.sourceforge.net/glew/glew-1.13.0.tgz?download")
    source = expand_tgz(archive)
    Dir.chdir(source) do
      # GLEW uses their own Makefile-system. It's easier to compile
      # it manually rather than try passing the correct flags to their
      # build system.
      system(
        "gcc",
        "-DGLEW_NO_GLU", "-DGLEW_STATIC",
        "-O2", "-Wall", "-W", "-Iinclude",
        "-dynamic", "-fno-common",
        "-o", "glew.o",
        "-c", "src/glew.c",
        *config.cxxflags(arch)
      )
      system("ar", "cr", "libGLEW.a", "glew.o") # *config.ldflags(arch)

      include_dir = install_path(arch, ["include", "GL"])
      lib_dir = install_path(arch, "lib")
      FileUtils.mkdir_p(lib_dir) if !File.exist?(lib_dir)
      FileUtils.mkdir_p(include_dir) if !File.exist?(include_dir)

      system("install", "-m", "0644", "libGLEW.a", lib_dir)
      system("install", "-m", "0644", "include/GL/glew.h", include_dir)      
    end
  }

  return libs
end

class Builder
  def initialize(lib_file=nil, &build_rules)
    @base_directory = File.expand_path(File.dirname(__FILE__))
    @lib_file = lib_file
    @build_rules = build_rules
  end

  def install(build_config)
    build_config.archs.each do |arch|
      instance_exec(build_config, arch, &@build_rules)
    end
    if @lib_file
      build_config.archs.each do |arch|
        lipo_info(install_path(arch, @lib_file))
      end
    end
  end

  def path(components=[], option=nil)
    p = File.join(@base_directory, *Array(components))
    case option
    when :make_dir
      FileUtils.mkdir_p(p) if !File.exist?(p)
    when :clear_dir
      FileUtils.rm_r(p) if File.exist?(p)
      FileUtils.mkdir_p(p)
    when :clear_file
      FileUtils.rm_r(p) if File.exist?(p)
      base_path = File.dirname(p)
      FileUtils.mkdir_p(base_path) if !File.exist?(base_path)
    end
    return p
  end

  def fetch(url)
    file_name = File.basename(URI.parse(url).path)
    base_path = self.path("work/archives", :make_dir)
    file_path = File.join(base_path, file_name)
    system("curl", "--location", "-o", file_path, url) if !File.exist?(file_path)
    return file_path
  end

  def open_dmg(path)
    file_name = File.basename(path)
    base_path = self.path("work/mounts", :make_dir)
    file_path = File.join(base_path, file_name)
    system("hdiutil", "attach", "-mountpoint", file_path, path)
    return file_path
  end

  def close_dmg(path)
    system("hdiutil", "detach", path)
  end

  def copy_framework(source, name)
    framework_name = "#{name}.framework"
    file_path = self.path("libraries/Frameworks/#{framework_name}", :clear_file)
    FileUtils.cp_r(File.join(source, framework_name), file_path)
    return file_path
  end

  def link_framework(framework_path, arch)
    framework_name = File.basename(framework_path)
    name = File.basename(framework_name, ".framework")

    # link lib file
    framework_lib_file = File.join(framework_path, name)
    system("lipo", framework_lib_file, "-verify_arch", arch)
    if $?.exitstatus == 0
      symlink_relative(framework_lib_file, self.path("libraries/#{arch}/lib/lib#{name}.dylib", :clear_file))
    else
      puts "Error: '#{framework_name}' is not built for '#{arch}'."
      exit(1)
    end

    # link headers
    header_directory = File.join(framework_path, "Headers")
    symlink_relative(header_directory, self.path("libraries/#{arch}/include/#{name}", :clear_file))
    Dir.glob("#{header_directory}/*") do |header_file|
      symlink_relative(header_file, self.path("libraries/#{arch}/include/AllFrameworks/#{File.basename(header_file)}", :clear_file))
    end
  end

  def expand_tgz(path)
    file_path = self.path(["work/sources", File.basename(path)], :clear_dir)
    system("tar", "xzf", path, "-C", file_path, "--strip-components", "1")
    return file_path
  end

  def escape_sh(str)
  	str.to_s.gsub(/(?=[^a-zA-Z0-9_.\/\-\x7F-\xFF\n])/n, '\\').gsub(/\n/, "'\n'").sub(/^$/, "''")
  end

  def lipo_info(path)
    puts "\n========== Running lipo -info #{path}"
    system("lipo", "-info", path)
  end

  def install_path(arch, components=[], option=nil)
    return self.path(["libraries", arch] + Array(components), option)
  end
  
  def symlink_relative(source, target)
    Dir.chdir(File.dirname(target)) do |target_dir|
      source_relative = Pathname.new(source).relative_path_from(Pathname.new(target_dir)).to_s
      FileUtils.ln_s(source_relative, File.basename(target))
    end
  end

  def apply_patch(patch_file)
    IO.popen("patch -p1", "w") do |io|
      io.puts(File.read(patch_file))
      io.close_write
    end
  end
end

class BuildConfiguration
  def initialize(archs, sdk, version_min)
    @archs = archs
    @sdk = sdk
    @version_min = version_min
  end

  attr_reader(:archs)

  def env_variables(arch)
    ENV["CXXFLAGS"] = cxxflags(arch).join(" ")
    ENV["CFLAGS"] = ENV["CXXFLAGS"]
    ENV["LDFLAGS"] = ldflags_full(arch).join(" ")
  end

  def cxxflags(arch)
    flags = ["-arch", arch, "-stdlib=libc++"]
    flags += ["-isysroot", @sdk] if !@sdk.empty?
    flags << "-mmacosx-version-min=#{@version_min}" if !@version_min.empty?
    return flags
  end

  def ldflags(arch)
    flags = []
    flags << "-Wl,-syslibroot,#{@sdk}" if !@sdk.empty?
    return flags
  end

  def ldflags_full(arch)
    flags = ldflags(arch)
    flags += cxxflags(arch)
    return flags
  end
end

def parse_options(libraries)
  options = {
    "libraries_to_install" => [],
    "archs" => [],
    "sdk" => "",
    "version_min" => ""
  }

  parser = OptionParser.new { |opts|
    opts.banner = "Usage: #{$0} [options] [libraries to install...]"

    opts.on("--arch ARCH", "A comma separated list of archs to build") do |value|
      options["archs"] += value.split(",")
    end

    opts.on("--sdk SDK", "A path to the developement SDK") do |value|
      options["sdk"] = value
    end

    opts.on("--version-min VERSION", "The minimum OS X version to build for") do |value|
      options["version_min"] = value
    end

    opts.on("-l", "--list", "List all installable libraries") do
      libraries.keys.sort.each { |name| puts name }
      exit(0)
    end

    opts.on("-a", "--all", "Install all libraries") do
      options["libraries_to_install"] += libraries.keys.sort
    end

    opts.on("-h", "--help", "Print this help message") do
      puts opts
      exit(0)
    end
  }

  begin
    parser.parse!
  rescue OptionParser::ParseError => e
    puts "Error: #{e.message}"
    exit(1)
  end

  options["libraries_to_install"] += ARGV

  if options["libraries_to_install"].empty?
    puts "Error: specify a library to install."
    exit(1)
  end

  if options["archs"].empty?
    puts "Error: specify the arch(s) that will be installed."
    exit(1)
  end

  return options
end

def main
  libraries = all_libraries()
  options = parse_options(libraries)
  build_config = BuildConfiguration.new(options["archs"], options["sdk"], options["version_min"])
  options["libraries_to_install"].each do |library_name|
    lib = libraries[library_name]
    if lib
      lib.install(build_config)
    else
      puts "Error: '#{library_name}' is not an installable library."
    end
  end
end

main() if __FILE__ == $PROGRAM_NAME
